import java.util.Scanner;
public class Roulette {
    public static void main(String[] args){
        RouletteWheel roulette = new RouletteWheel();
        java.util.Scanner reader = new Scanner(System.in);
        int money = 1000;
        boolean play = true;

        while(play){
            System.out.println("You currently have: "+money+"$, would you like to bet?  (y/n)");
            String answer = reader.next();
            if(answer.equals("y")){
                boolean validBet = false;
                int betPlaced = 0;
                while(!validBet){
                    System.out.println("How much will you bet?");
                    betPlaced = reader.nextInt();
                    if(betPlaced <= money){
                    validBet = true;
                    }
                    else{
                        System.out.println("Invalid bet, re-enter");
                        }
                }
                boolean validNum = false;
                int numChosen = 0;
                while(!validNum){
                    System.out.println("What number do you want to bet on?");
                    numChosen = reader.nextInt();
                    if(numChosen>=0 && numChosen < 37){
                        validNum = true;
                    }
                    else{
                        System.out.println("Invalid number, choose between 0 and 36");
                    }
                }
                roulette.spin();
                int result = roulette.getValue();
                System.out.println("Result is: "+result);
                if(checkWin(numChosen, result)){
                    System.out.println("You won!");
                    money = money+(betPlaced+(betPlaced*35));
                }
                else{
                    System.out.println("You lost...");
                    money = money-betPlaced;
                }
            }
            else if(answer.equals("n")){
                int endResult = (money-1000);
                System.out.println("Thanks for playing. Final cashout: "+endResult);
                play = false;
            }
        }
    }

    public static boolean checkWin(int numChosen, int rouletteVal){
        if(rouletteVal == numChosen){
            return true;
        }
        else {
            return false;
        }
    }
}
