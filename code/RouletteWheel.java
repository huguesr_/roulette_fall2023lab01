import java.util.Random;
public class RouletteWheel {
    private Random randGen;
    private int lastNum;

    public RouletteWheel(){
        randGen = new Random();
        lastNum = 0;
    }

    public void spin(){
        lastNum = randGen.nextInt(37);
    }

    public int getValue(){
        return lastNum;
    }
}
